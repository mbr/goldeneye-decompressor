﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.IO.Compression;


namespace GEDec
{
    public class GEDataFile
    {
        /// <summary>
        /// Offset in the Goldeneye US ROM for the data file.
        /// </summary>
        public const int DataFileOffset = 0x21990;
        public const int DataFileLength = 0x11840;

        /// <summary>
        /// Decompressed contents of the Goldeneye data file.
        /// </summary>
        public byte[] Contents
        {
            get;
            set;
        }


        public GEFileTable FileTable
        {
            get;
            set;
        }


        public GEDataFile(Stream rom)
        {
            // Seek to start of data file (adding 2 to skip the 0x1172 magic)
            rom.Seek(DataFileOffset + 2, SeekOrigin.Begin);

            var destrm = new DeflateStream(rom, CompressionMode.Decompress);
            var tgt = new MemoryStream();
            destrm.CopyTo(tgt);
            Contents = tgt.ToArray();

            // Load file table
            FileTable = new GEFileTable(Contents);
        }
    }
}
