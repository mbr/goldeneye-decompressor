﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GEDec
{
    public class GEFileEntry
    {
        private static uint swapEndianness(uint x)
        {
            return ((x & 0x000000ff) << 24) +  // First byte
                   ((x & 0x0000ff00) << 8) +   // Second byte
                   ((x & 0x00ff0000) >> 8) +   // Third byte
                   ((x & 0xff000000) >> 24);   // Fourth byte
        }

        public UInt32 UnknAddr
        {
            get;
            set;
        }

        public UInt32 Start
        {
            get;
            set;
        }

        public UInt32 Index
        {
            get;
            set;
        }


        public const int EntrySize = 12;

        public override string ToString()
        {
            return String.Format("{0:d4} Start: {1:x8} Info: {2:x8}", Index, Start, UnknAddr);
        }

        public GEFileEntry (byte[] source, int idx)
        {
            UnknAddr = swapEndianness(BitConverter.ToUInt32(source, idx + 0));
            Start = swapEndianness(BitConverter.ToUInt32(source, idx + 4));
            Index = swapEndianness(BitConverter.ToUInt32(source, idx + 8));
        }
    }
}
