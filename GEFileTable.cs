﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GEDec
{
    public class GEFileTable
        : List<GEFileEntry>
    {
        /// <summary>
        /// Offset in Goldeneye data file where the ROM offset table resides.
        /// </summary>
        private const int DataOffset = 0x252C8;

        /// <summary>
        /// End offset in the Goldeney data file for the ROM offset table.
        /// </summary>
        private const int DataEnd = 0x274D8;

        public GEFileTable(byte[] data)
            : base()
        {
            for (int i = DataOffset; i < DataEnd; i += GEFileEntry.EntrySize)
                Add(new GEFileEntry(data, i));
        }
    }
}
