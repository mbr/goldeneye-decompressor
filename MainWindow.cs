﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.IO.Compression;


namespace GEDec
{
    public partial class MainWindow : Form
    {
        public MainWindow()
        {
            InitializeComponent();

            textBox2.Text = Environment.CurrentDirectory + @"\output";
        }


        private void PerformExtraction ( )
        {
            int idx = 0, count = 0, decCount = 0, rawCount = 0;
            var rom = File.Open(textBox1.Text, FileMode.Open);

            Directory.CreateDirectory(textBox2.Text);

            var datafile = new GEDataFile(rom);

            foreach( var entry in datafile.FileTable )
            {
                byte[] header = new byte[2];

                if( entry.Start == 0 )
                {
                    idx++;
                    continue;
                }

                rom.Seek(entry.Start, SeekOrigin.Begin);
                rom.Read(header, 0, 2);

                // No decompression
                if( (header[0] << 8 | header[1]) != 0x1172  )
                {
                    if (idx + 1 != datafile.FileTable.Count)
                    {
                        var output = File.OpenWrite(textBox2.Text + @"\" + String.Format("{0:X8}.bin", entry.Start));
                        int size = (int)(datafile.FileTable[idx + 1].Start - entry.Start);
                        var buffer = new byte[size];

                        rom.Seek(entry.Start, SeekOrigin.Begin);
                        rom.Read(buffer, 0, size);
                        output.Write(buffer, 0, size);

                        output.Close();

                        rawCount++;
                    }
                }
                // Decompress
                else
                {
                    var deflate = new DeflateStream(rom, CompressionMode.Decompress);
                    var output = File.OpenWrite(textBox2.Text + @"\" + String.Format("{0:X8}.dec.bin",entry.Start));
                    deflate.CopyTo(output);

                    output.Close();

                    decCount++;
                }
                
                idx++;
                count++;
            }

            rom.Close();

            MessageBox.Show(
                "Extracted " + count + " files; decompressed " + decCount + " and straight-copied " + rawCount + "." + Environment.NewLine,
                "Finished"
            );

            System.Diagnostics.Process.Start(textBox2.Text);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var d = new OpenFileDialog();

            if( d.ShowDialog() == System.Windows.Forms.DialogResult.OK )
                textBox1.Text = d.FileName;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var d = new FolderBrowserDialog();

            d.SelectedPath = Environment.CurrentDirectory;

            if (d.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                textBox2.Text = d.SelectedPath;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            (sender as Button).Enabled = false;

            try
            {
                PerformExtraction();
            }
            catch( Exception error )
            {
                MessageBox.Show(
                    error.Message + Environment.NewLine + Environment.NewLine + error.StackTrace,
                    "Error while performing extraction"
                );
            }
                

            (sender as Button).Enabled = true;
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var w = new AboutBox1();
            w.ShowDialog();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }
    }
}
