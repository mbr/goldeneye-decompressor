# README #

Choose your ROM file (make sure it's the stock US Goldeneye ROM; md5: 70c525880240c1e838b8b1be35666c3b) and choose an output directory:

![snap1.png](https://bitbucket.org/repo/Gp65Xa/images/1981529467-snap1.png)

Click "Decompress". When the program is finished a dialog box will pop up:

![snap2.png](https://bitbucket.org/repo/Gp65Xa/images/4290692693-snap2.png)

And it will open the newly created folder with the extracted files:

![snap3.png](https://bitbucket.org/repo/Gp65Xa/images/466311450-snap3.png)